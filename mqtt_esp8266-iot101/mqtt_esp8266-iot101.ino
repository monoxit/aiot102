/*
 * pubsubclientライブラリに添付のスケッチ例 mqtt_esp8266を基に修正
*/

// ESP8266 WiFi ライブラリとPubSubClient(MQTTクライアント）ライブラリを使うと指定
// ライブラリ：ある機能を容易に使うことができるようにあらかじめ作られたプログラム集
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// ネットワーク環境に合わせて修正

const char* ssid = "YOUR_SSID";
const char* password = "???";
const char* mqtt_server = "mqtt.example.com";

WiFiClient espClient;
// MQTTクライアントライブラリを「client」という名前で使えるようにする
PubSubClient client(espClient);
unsigned long lastMsg = 0;
char msg[50];
int value = 0;

void setup() {
  WiFi.mode(WIFI_STA);
  pinMode(13, OUTPUT);     // 13番ピンを出力に設定
  Serial.begin(115200);
  // WiFiにつなぐ
  setup_wifi();
  // MQTTクライアント機能に、接続先アドレスとポート番号を設定
  client.setServer(mqtt_server, 1883);
  // MQTTクライアント機能に、サーバーからメッセージを受信したときに実行する部分を設定
  client.setCallback(callback);
}

// WiFiにつなぐ部分
// プログラムの他の部分にsetup_wifi()があると実際はここの{から}が実行される
void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// MQTTサーバーからメッセージを受信したときに実行させる部分
void callback(char* topic, byte* payload, unsigned int length) {
  // Serial・・・の行はプログラムの主要部分でない
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // 先頭が「1」のメッセージを受信するとLEDをONにする
  if ((char)payload[0] == '1') {
    digitalWrite(13, HIGH);
  } else { //でなければ
    digitalWrite(13, LOW);
  }

}

// MQTTサーバーへ接続する部分
void reconnect() {
  // 接続できるまで繰り返す
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // デバイスID 「IOT001」で接続開始
    if (client.connect("IOT888")) {
      Serial.println("connected");
      // 接続できたらトピック「outTopic」に
      //「IOT001:hello world」を投稿
      client.publish("outTopic", "IOT001:hello world");
      // ...さらにトピック「inTopic」に申し込み
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// ずっと繰り返す部分
void loop() {

  // MQTTサーバーに接続していなかったらreconnect()の部分を実行
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  
  unsigned long now = millis();
  if (now - lastMsg > 2000UL) {
    //最後のメッセージ送信から2000ミリ秒以上経過していたら
    lastMsg = now;
    ++value; // valueに１を加算する
    // snprintf命令でメッセージの%ldのところにvalueを埋め込み、msgにセット
    snprintf (msg, sizeof(msg), "IOT001:hello world #%ld", value);
    Serial.print("Publish message: ");
    Serial.println(msg);
    //トピック「outTopic」にメッセージを投稿
    client.publish("outTopic", msg);
  }
}