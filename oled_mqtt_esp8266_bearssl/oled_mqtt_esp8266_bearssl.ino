/*
  Basic ESP8266 MQTT example with OLED
    Copyright (c) 2019 Masami Yamakawa
    setupWifi(), reconnect() and parts of callback() functions are
      Copyright (c) 2008-2015 Nicholas O'Leary
    This software is released under the MIT License.
    http://opensource.org/licenses/mit-license.php
    ESP8266 core >= v2.5.0
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <ArduinoJson.h>
#include "images.h"

// mosquitto, eclipse or hivemq
#define eclipse

// 環境に合わせて変更
const char* ssid = "YOUR_WIFI_SSID";
const char* password = "YOUR_WIFI_PASSWORD";
const String mqttTopicPrefix = "mxitaiot/rpi998";

// MQTTデバイスIDのプレフィックス
// プレフィックスの後にESP8266のデバイスIDが付加されMQTTのデバイスIDが作られる
// 「test-」としたときは「test-30f3ab」のようなIDとなる
const char* mqttDeviceIdPrefix = "test-";

// 拇印(fingerprint)は2019年3月確認　再確認の必要あり
#ifdef mosquitto
const char* mqttServer = "test.mosquitto.org";
const char* mqttFingerprint = "e62d6f0d957ed20b74b1d55d404eef992ce482f0"; //test.mosquitto.orgの拇印
const int   mqttPort = 8883;
#endif

#ifdef eclipse
const char* mqttServer = "iot.eclipse.org";
const char* mqttFingerprint = "1C:AA:5E:0F:3F:6F:1D:CB:9E:07:FB:34:53:32:8D:04:49:71:39:13"; //iot.eclipse.orgの拇印
const int   mqttPort = 8883;
#endif

#ifdef hivemq
const char*  mqttServer = "broker.hivemq.com";
const int   mqttPort = 1883;
#endif

const int screenWidth = 128; // OLED 横 (pixels)
const int screenHeight = 64; // OLED 縦 (pixels)
const int oledReset = -1; // リセットピン（無しの場合ｈ-1）
Adafruit_SSD1306 display(screenWidth, screenHeight, &Wire, oledReset);

//SSL使用時 WiFiClientSecure
#ifdef hivemq
WiFiClient espClient;
#else
WiFiClientSecure espClient;
#endif

//MQTTクライアントライブラリをclientという名前で使う
PubSubClient client(espClient);

// JSON形式データを扱うためのメモリをdocという名前で確保
StaticJsonDocument<200> doc;

String mqttDeviceId;

void setup() {
  Serial.begin(115200);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }
  //display.display();
  //delay(2000);

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);

  // ESP8266のデバイスIDを付加したMQTTクライアントIDを作る
  mqttDeviceId = String(mqttDeviceIdPrefix) + String(ESP.getChipId(), HEX);
  display.print("ID:");
  display.println(mqttDeviceId);

  setupWifi();

  espClient.allowSelfSignedCerts();
  espClient.setFingerprint(mqttFingerprint);

  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
}

void setupWifi() {

  delay(10);
  // We start by connecting to a WiFi network
  display.print("WiFi:");
  display.print(ssid);
  display.display();
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    display.print(".");
    display.display();
  }

  display.println("");
  display.print("IP:");
  display.println(WiFi.localIP());
  display.display();
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  deserializeJson(doc, payload);

  int numHeart = doc["id"];
  int x = doc["x"];
  int y = doc["y"];

  Serial.print("id:");
  Serial.println(numHeart);
  Serial.print("x:");
  Serial.println(x);
  Serial.print("y:");
  Serial.println(y);
  // 数だけハートを表示する
  if (numHeart > 3) numHeart = 3; //最大3に制限
  display.clearDisplay();
  display.setCursor(0, 0);
  for (int i = 0; i < numHeart; i++) {
    display.drawBitmap(i * 42, 32, heart_bmp, HEART_WIDTH, HEART_HEIGHT, 1);
  }
  if (numHeart > 0 && x > 150) display.invertDisplay(true);
  else display.invertDisplay(false);
  display.display();

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    display.print("MQTT:");
    display.print(mqttServer);
    display.display();
    // Attempt to connect
    if (client.connect(mqttDeviceId.c_str())) {
      display.println(" connected");
      display.display();
#ifndef hivemq
      if (!espClient.verify(mqttFingerprint, mqttServer)) {
        display.println("Bad certificate!!");
        display.display();
        //delay(5000);
        //return;
      }
#endif
      // MQTTブローカーと接続できたら状態をパブリッシュ（動作確認をしやすくするため）
      String mqttTopic = mqttTopicPrefix + "/status";
      String mqttPayload = mqttDeviceId + " started";
      client.publish(mqttTopic.c_str(), mqttPayload.c_str());
      // さらに/gestureトピックへ申し込み
      mqttTopic = mqttTopicPrefix + "/gesture";
      client.subscribe(mqttTopic.c_str());
    } else {
      display.print("failed!!, rc=");
      display.println(client.state());
      display.display();
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

}
